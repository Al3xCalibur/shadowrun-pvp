# ShadowRun PvP

All ShadowRun PvP's files to improve the experience

## How to install it
clone the project

go to app/client and run `npm install`

go to app/server and run `npm install`

## How to run it

go to app/client and run `npm run serve`

go to app/server and run `npm run start`    
