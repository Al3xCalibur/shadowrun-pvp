import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/components/Login'
import MapMJ from '@/components/MapMJ'
import PNJ from '@/components/PNJ'
import PJ from "@/components/PJ"
import Map from '@/components/Map'

Vue.use(Router)

const routes = [
    {
        path: '/',
        name: 'login',
        component: Login
    },
    {
        path:'/mj',
        name: 'mapmj',
        component: MapMJ,
        meta: { requiresAuth: true, mjAuth: true, pjAuth: false }
    },
    {
        path: '/pj/:name',
        name: 'pj',
        component: PJ,
        meta: { requiresAuth: true, mjAuth: true, pjAuth: true}
    },
    {
        path: '/pj',
        name: 'pj',
        component: PJ,
        meta: { requiresAuth: true, mjAuth: true, pjAuth: true }
    },
    {
        path: '/pnj',
        name: 'pnj',
        component: PNJ,
        meta: { requiresAuth: true, mjAuth: true, pjAuth: false }
    },
    {
        path: '/map/:name',
        name: 'map',
        component: Map,
        meta: { requiresAuth: true, mjAuth: true, pjAuth: false }
    }
]

const router = new Router({routes})

router.beforeEach((to, from, next) => {
    if(to.meta.requiresAuth){
        const token = localStorage.getItem("token")
        if(!token){
            next({name: 'login'})
        } else {
            if (to.meta.mjAuth && localStorage.getItem("role") === "mj") {
                next()
            } else if (to.meta.pjAuth && localStorage.getItem("role") === "pj") {
                next()
            } else {
                next({name: 'login'})
            }
        }
    } else {
        next()
    }
})

export default router
