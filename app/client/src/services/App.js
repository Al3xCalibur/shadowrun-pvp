import Api from '@/services/Api'
import Axios from "axios"

export default {
    fetchMjs(){
        return Api().get('mjs')
    },
    async login(data){
        return Api().post('login', data).then(function (res){
            if (res.status === 200 && 'token' in res.data) {
                localStorage.setItem('token', res.data.token)
                for(let prop of Object.keys(data)){
                    localStorage.setItem(prop, data[prop])
                }
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.token
                return true
            }
            return false
        })
    },
    async loginMj(username){
        return this.login({role: "mj", username: username})
    },
    async loginPj(username, mj) {
        return this.login({role: "pj", username: username, mj: mj})
    }
}
