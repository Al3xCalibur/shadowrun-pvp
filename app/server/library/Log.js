class Log {
    constructor(location, room, value = "", type = "texte"){
        this.location = location
        this.room = room
        this.type = type
        this.value = value
    }
}

module.exports = Log
