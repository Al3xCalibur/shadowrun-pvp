class Floor {
    constructor(name, pjs = {}, pnjs= {}, interests = {}, src= ""){
        this.name = name
        this.pjs = pjs
        this.pnjs = pnjs
        this.interests = interests
        this.src = src
    }
}

module.exports = Floor
