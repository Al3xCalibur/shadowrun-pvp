let states = ["ok", "bof", "bad", "die"]
class Enhancement {
    constructor(name, location, state = 0, details = ""){
        this.name = name
        this.location = location
        this.state = states[state%(states.length)]
        this.details = details
    }

    cycleState(){
        this.state = states[(states.indexOf(this.state)+1) % (states.length)]
    }
}

if(typeof module !== "undefined") module.exports = Enhancement
