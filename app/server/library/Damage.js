class Damage{
    constructor(physicalDamage, stunnedDamage){
        this.maxPhysical = physicalDamage
        this.maxStunned = stunnedDamage
        this.physical = 0
        this.stunned = 0
        this.malus = 0
    }

    addPhy(dmg){
        this.physical += dmg
        this.malus = this.getMalus()
    }

    addStu(dmg){
        this.stunned += dmg
        this.malus = this.getMalus()
    }

    getMalus(){
        return Math.floor(this.physical / 3) + Math.floor(this.stunned / 3)
    }
}

if(typeof module !== "undefined") module.exports = Damage

