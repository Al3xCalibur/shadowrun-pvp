const Damage = require('../library/Damage')

class PJ {
    constructor(name, physicalDamage, stunnedDamage, enhancements = [], initiative = 0, other = null){
        this.name = name
        this.damage = new Damage(physicalDamage, stunnedDamage)
        this.enhancements = enhancements
        this.initiative = initiative
        this.other = other
    }

    addEnhancement(enhancement){
        this.enhancements.push(enhancement)
    }

    removeEnhancement(enhancement){
        this.enhancements.splice(this.enhancements.indexOf(enhancement),1)
    }
}

if(typeof module !== "undefined") module.exports = PJ
