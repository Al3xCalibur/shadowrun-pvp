class Interest {
    constructor(name, description, equipeA = false, equipeB = false, note = ""){
        this.name = name
        this.description = description
        this.equipeA = equipeA
        this.equipeB = equipeB
        this.note = note
    }
}

module.exports = Interest
