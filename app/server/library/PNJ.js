let states = ["ok", "bof", "bad", "mort"]

class PNJ {
    constructor(name, state = 0, func = "", physical = "", mentality = "", note = ""){
        this.name = name
        this.state = states[state%(states.length)]
        this.note = note
        this.function = func
        this.physical = physical
        this.mentality = mentality
    }

    cycleState(){
        this.state = states[(states.indexOf(this.state)+1) % (states.length)]
    }
}

PNJ.states = states

if(typeof module !== "undefined") module.exports = PNJ
