class MJ {
    constructor(name, time = 0, isBattling = false, pjs = [], map = ""){
        this.name = name
        this.time = time
        this.isBattling = isBattling
        this.pjs = pjs
        this.map = map
    }

    addTime(time){
        this.time += time
    }

    toggleBattling(){
        this.isBattling = !this.isBattling
    }

    addPj(pj){
        if(!this.pjs.includes(pj))
            this.pjs.push(pj)
    }

    getPj(name){
        for(let x of this.pjs){
            if(x.name === name){
                return x
            }
        }
        return null
    }

}

if(typeof module !== "undefined") module.exports = MJ
