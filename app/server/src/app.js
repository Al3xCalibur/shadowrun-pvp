const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

const jwt = require('jsonwebtoken');
let secretKey = "A2m!;:,ezfAJNI512nbvi55dobdi"

const MJ = require('../library/MJ')
const PJ = require('../library/PJ')
const PNJ = require('../library/PNJ')
const Enhancement = require('../library/Enhancement')
const Floor = require('../library/Floor')
const Position = require('../library/Position')
const infos = require('../src/info')

let mjs = {}
let pjs = {}
let pnjs = {}
let floors = {}
/*let floors = {
    "1": new Floor("1", {"Lol": new Position(0,0), "Alex": new Position(0,0)}),
    "2": new Floor("2"),
    "3": new Floor("3"),
}*/

for(let x of infos.pnjs){
    let name = x["nom"]
    let func = x["fonction"]
    let state = x["etat"]
    let phy = x["desc_phy"]
    let mental = x["mentalite"]
    let notes = x["divers"]
    pnjs[name] = new PNJ(name, state,func,phy,mental,notes)
}

for(let x of infos.floors){
    let name = x["nom"]
    let desc = x["desc"]
    let notes = x["divers"]
    let pnjs = x["pnjs"]
    let src = x["src"]
    let res = {}
    for(let pnj of pnjs){
        res[pnj] = new Position(500,500)
    }
    floors[name] = new Floor(name, {}, res, {},src)
}

pjs["golem"] = new PJ("golem", 18, 10, [
],
    0
    )

pjs["ezra"] = new PJ("ezra", 14, 10, [

    ],
    0
)

pjs["roots"] = new PJ("roots", 12, 11, [

    ],
    0
)
pjs["hackerman"] = new PJ("hackerman", 14, 10, [

    ],
    0
)

//mjs["Bwuljqh"].map = "Death"
pjs["Mac Simus"] = new PJ("Mac Simus", 11,10, [

    ],
    0
)

pjs["Patch"] = new PJ("Patch", 18, 9, [

    ],
    0
)

pjs["Kera"] = new PJ("Kera", 14, 12, [

    ],
    0
)

pjs["Circe"] = new PJ("Circe", 14, 11, [

    ],
    0
)

mjs["Bwuljqh"] = new MJ("Bwuljqh",0,false, [pjs["Kera"], pjs["ezra"], pjs["roots"], pjs["hackerman"]])

mjs["Mockie"] = new MJ("Mockie",0, false, [pjs["Patch"], pjs["golem"]])

mjs["Bido"] = new MJ("Bido",0, false, [pjs["Circe"], pjs["Mac Simus"]])
mjs["Sertet"] = new MJ("Sertet", 0, false)
mjs["Fahro"] = new MJ("Fahro", 0, false)
mjs["Random"] = new MJ("Random", 0, false)
const server = app.listen(process.env.PORT || 8081)
const io = require('socket.io')(server);



/*app.get('/mj-available-name', function(req,res){
    res.json({availableName: !mjs.hasOwnProperty(req.session.username)})
})*/


app.post('/login', function(req, res){
    if(req.body.role === 'mj' && mjs.hasOwnProperty(req.body.username)) {
        res.json({token: jwt.sign(JSON.stringify(req.body), secretKey)})
    } else if(req.body.role === 'pj' && mjs.hasOwnProperty(req.body.mj) /* && !pjs.hasOwnProperty(req.body.username)*/){
        if(pjs[req.body.username] === null) {
            pjs[req.body.username] = new PJ(req.body.username, 0)
        }
        mjs[req.body.mj].addPj(pjs[req.body.username])
        res.json({token: jwt.sign(JSON.stringify(req.body), secretKey)})
    } else {
        res.status(401).json({error: "Already registered"})
    }
})

app.post('/pj', function(req, res){
    if(req.body && req.body.username) {
        res.json(pjs[req.body.username])
    } else {
        res.status(401).json()
    }
})

app.get('/mjs', function(req, res){
    res.json(Object.keys(mjs))
})


let mjNamespace = io.of('/mj')
mjNamespace.use(authenticate)
.on("connection", function (socket) {
    if(!socket.mj || !mjs.hasOwnProperty(socket.decoded)) {
        socket.disconnect()
    }
    console.log("Mj "+socket.decoded+" connected")

    acknowledgeAll(mjs)

    socket.on('addTime', function(data){
        mjs[socket.decoded].addTime(data)
        acknowledgeAll(mjs)
    })

    socket.on('toggleBattling', function(data){
        mjs[socket.decoded].toggleBattling()
        acknowledgeAll(mjs)
    })

    socket.on('addInit', function(data){
        let pj = mjs[socket.decoded].getPj(data.name)
        if(pj != null){
            pj.initiative += data.value
            socket.emit('update', mjs)
            pjNamespace.to(pj.name).emit('update', pj)
        }
    })

    socket.on('addPhy', function(data){
        let pj = mjs[socket.decoded].getPj(data.name)
        if(pj != null){
            pj.damage.addPhy(data.value)
            socket.emit('update', mjs)
            pjNamespace.to(pj.name).emit('update', pj)
        }
    })

    socket.on('addStu', function(data){
        let pj = mjs[socket.decoded].getPj(data.name)
        if(pj != null){
            pj.damage.addStu(data.value)
            socket.emit('update', mjs)
            pjNamespace.to(pj.name).emit('update', pj)
        }
    })

    socket.on('turnInit', function(data){
        for(let x of mjs[socket.decoded].pjs) {
            x.initiative -= data
            pjNamespace.to(x.name).emit('update', x)
        }
        socket.emit('update', mjs)
    })

    // socket.on('addPJ', function(data){
    //
    // })

    socket.on('help', function(data){
        let time = new Date()
        let hour = time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds()
        console.warn(hour+" "+ mjs[socket.decoded].name+" : HELP !!!")
    })

    socket.on('disconnect', function(){
        //TODO: do not remove but only do not display it on the first menu or save state for a better reco
        // if(mjs[socket.decoded])
        //     delete mjs[mjs[socket.decoded].name]
    })
    
    socket.on('reconnect', function(){
        if(!socket.mj || !mjs.hasOwnProperty(socket.decoded)){
            socket.disconnect()
        }
        acknowledgeAll(mjs)
    })
})

function acknowledgeOthers(socket, data){
    socket.broadcast.emit('update', data)
}

function acknowledgeAll(data, event = "update"){
    mjNamespace.emit(event, data)
}


let pjNamespace = io.of('/pj')
pjNamespace.use(authenticate)
.on('connection', function(socket){
    let data = socket.decoded
    if(socket.mj || (pjs[data] != null && pjs[data].other != null && pjs[data].other.name === socket.handshake.query.username)) {
        data = socket.handshake.query.username
        console.log(socket.decoded, "connected to look for", data)
    } else {
        console.log(data, "connected")
    }



    socket.join(data)
    if(pjs.hasOwnProperty(data)) {
        let mj = getMjFromPj(data)
        pjNamespace.to(data).emit('update', pjs[data])
        if(mj != null)
            pjNamespace.to(mj.name).emit('update', mj.pjs)
    }


    socket.on('subscribe', function(data){
        socket.names = data

        socket.join(data)
        let res = []
        for(let x of data)
            res.push(pjs[x])
        socket.emit('update', res)
    })

    socket.on('state', function(data){
        let enhancements = pjs[data.username].enhancements
        for(let enhancement of enhancements){
            if(enhancement.name === data.enhancement){
                enhancement.cycleState()

                pjNamespace.to(data.username).emit('update', pjs[data.username])
                return
            }
        }
    })

    socket.on('setInit', function(x){
        pjs[data].initiative = x
        pjNamespace.to(data).emit('update', pjs[data])
        mjNamespace.emit('update', mjs)
    })

    socket.on('disconnect', function(){
        // if(subscribed[socket.name])
        //     subscribed[socket.name].splice(subscribed[socket.name].indexOf(socket), 1)
    })

    socket.on('reconnect', function(){
        // subscribed[socket.name].push(socket)
        socket.emit('update', pjs[socket.name])
    })

})


let pnjNamespace = io.of('/pnj')
pnjNamespace.use(authenticate)
.on('connection', function(socket){
    if(socket.mj) {
        socket.emit('update', Object.values(pnjs))

        socket.on('state', function (name) {
            if (pnjs.hasOwnProperty(name)) {
                pnjs[name].cycleState()
                socket.emit('update', Object.values(pnjs))
            }
        })
    }
})


let mapNamespace = io.of('/map')
mapNamespace.use(authenticate)
.on('connection', function(socket){

    if(socket.handshake.query.name !== undefined){
        let mj = mjs[socket.handshake.query.name]
        if(mj){
            socket.join(mj.name)
            socket.emit('update', floors[mj.map])
            if(mj.floodFills !== undefined && mj.floodFills[mj.map] !== undefined)
                socket.emit('floodFill', mj.floodFills[mj.map])
        }
    } else {
        socket.emit('maps', Object.values(floors))
        let mj = mjs[socket.decoded]
        let floor = floors["Death"]
        if(mj !== undefined && mj.map !== undefined){
            let f = floors[mj.map]
            if(f != null)
                floor = f
        }
        socket.emit('update', floor)
        socket.join(floor.name)
        mj.map = floor.name
        if(mj.floodFills !== undefined && mj.floodFills[mj.map] !== undefined)
            socket.emit('floodFill', mj.floodFills[mj.map])
    }

    socket.on('updatePos', function (data) {
        let position;
        let floor;
        if(data.type === "pj" || data.type === "other"){
            floor = getFloorFromPj(data.name)
            position = floor.pjs[data.name]
        } else if(data.type === "pnj") {
            //floor = getFloorFromPnj(data.name)
            floor = floors[data.map]
            console.log(floor.name, data.name)
            position = floor.pnjs[data.name]
        } else if(data.type === "interest"){
            floor = getFloorFromInterest(data.name)
            position = floor.interests[data.name]
        }
        position.x = data.x
        position.y = data.y
        mapNamespace.to(floor.name).emit('update', floor)
        mapNamespace.to(socket.decoded).emit('update', floor)
    })

    socket.on('changeMapPj', function(data){
        let oldFloor = getFloorFromPj(data.name)
        let mj = mjs[socket.decoded]

        if(oldFloor) {
            delete oldFloor.pjs[data.name]
            if(mj.map === oldFloor.name)
                mapNamespace.to(mj.name).emit('update', oldFloor)
            mapNamespace.to(oldFloor.name).emit('update', oldFloor)
        }

        floors[data.map].pjs[data.name] = new Position(0,0)
        mapNamespace.to(data.map).emit('update', floors[data.map])
        if(mj.map === data.map)
            mapNamespace.to(mj.name).emit('update', floors[data])

    })

    socket.on('changeMap', function(data){
        socket.leave(mjs[socket.decoded].map, undefined)
        socket.join(data)

        socket.emit('update', floors[data])
        mapNamespace.to(socket.decoded).emit('update', floors[data])
        mjs[socket.decoded].map = data
        if(mjs[socket.decoded].floodFills !== undefined && mjs[socket.decoded].floodFills[data] != null) {
            socket.emit('floodFill', mjs[socket.decoded].floodFills[data])
            mapNamespace.to(socket.decoded).emit('floodFill', mjs[socket.decoded].floodFills[data])
        }
    })

    socket.on('floodfill', function(data){
        let mj = mjs[socket.decoded]
        if(mj.floodFills === undefined)
            mj.floodFills = {}
        if(mj.floodFills[mj.map] === undefined)
            mj.floodFills[mj.map] = []
        mj.floodFills[mj.map].push(data)
        mapNamespace.to(mj.name).emit('floodFill', [data])
    })

    socket.on('resetFog', function(){
        mjs[socket.decoded].floodFills = {}
        mapNamespace.to(socket.decoded).emit('resetFog')
    })
})



function authenticate(socket, next){
    if (socket.handshake.query && socket.handshake.query.token){
        jwt.verify(socket.handshake.query.token, secretKey, function(err, decoded) {
            if(err) return next(new Error('Authentication error'));
            socket.decoded = decoded.username
            socket.mj = decoded.role === "mj";
            next();
        })
    } else {
        next(new Error('Authentication error'));
    }
}


function getFloorFromPj(name){
    for(let x in floors){
        for(let pj in floors[x].pjs){
            if(pj === name)
                return floors[x]
        }
    }
}

function getFloorFromInterest(name){
    for(let x in floors){
        for(let pj of floors[x].interests){
            if(pj === name)
                return floors[x]
        }
    }
}

function getFloorFromPnj(name){
    for(let x in floors){
        for(let pj in floors[x].pnjs){
            if(pj === name)
                return floors[x]
        }
    }
}


function getMjFromPj(name){
    for(let x in mjs){
        for(let pj of mjs[x].pjs) {
            if (pj != null && pj.name === name)
                return mjs[x]
        }
    }
    return null
}
