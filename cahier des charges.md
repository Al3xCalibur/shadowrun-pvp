# Cahier des charges

MH = Must Have
GH = Good to Have
OH = Ok to Have

## MJ
- Cartes avec annotations: 
	- emplacement PJ/PNJ(a prendre de la liste) MH
	- notes ponctuelles(ping avec un texte) MH
	- notes de description de la salle(pièce) fixe et temps réel OH

- Gestion MH:
	- cadran heure des 2 tables
	- buttons pour gérer sa table +5min/-5min
	- Bouton Pause/Need Help
	- toggle savoir si en combat ou pas

- Listes PNJ MH:
	- a chaque PNJ associer moniteur pour savoir état rencontre/mort/notes les 2 tables sont synchros

- Listes PJ :
    - gérer initiative (boutons -5 ou -10 sur chaque persos et -10 sur l'ensemble)

## PJ
- Cartes avec annotations:
	- même chose que MJ mais a rajouté un toggle d'affichage des infos MH
	- PJ de sa propre table tjrs affiché MH
	- FOG of WAR de sa table GH
	- Changer la carte affichée OH

- Fiche de perso, joueur visible:
	- Santé localisée(MJ gère avec toggle affichage) GH
	- PV et stats(MJ gère aussi) MH
	- Accès fiche d'un autre joueur(MJ donne les droits) OH

- Accès à certains logs
